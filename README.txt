 * Library Name : VEGA_WiFiNINA by VEGA-Processor
 
This is a library for the WiFiNiNA Module SPECIFICALLY
FOR USE WITH VEGA ARIES Boards

Enables network connection (local and Internet) with VEGA ARIES IoT, ARIES v2, ARIES v3, ARIES MICRO

 * WiFiNiNA Datasheet: https://content.u-blox.com/sites/default/files/NINA-W10_DataSheet_UBX-17065507.pdf
 * ARIES Pinmap: https://vegaprocessors.in/files/PINOUT_ARIES%20V2.0_.pdf

Check out the above links for WiFiNiNA datasheet and ARIES Pinmap. 
 
 * Connections:
 ----------------------------
 * WiFiNiNA      Aries Board
 ----------------------------
 * VCC          -   3.3V
 * GND          -   GND
 * SPI_V_MISO   -   MOSI0
 * SPI_V_MOSI   -   MISO0
 * SPI_V_CLK    -   SCLK0
 * SPI_V_CS     -   SS0
 * RESET_N      -   GPIO-14
 * SYS_BOOT     -   GPIO-13
 * GPIO_7       -   GPIO-15
 ---------------------------

Other Useful links:

 * Official site: https://vegaprocessors.in/
 * YouTube channel: https://www.youtube.com/@VEGAProcessors
